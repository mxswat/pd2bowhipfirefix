BowWeaponBase = BowWeaponBase or class(ProjectileWeaponBase)

function BowWeaponBase:manages_steelsight()
	return false
end

local first_click_start = nil
local mouse_delay = 0.3

function BowWeaponBase:enter_steelsight_speed_multiplier()
    if first_click_start and first_click_start + mouse_delay > managers.player:player_timer():time() then
        if self._charging then
            first_click_start = nil
            self._cancelled = true
    
            self:play_tweak_data_sound("charge_cancel")
        end
    else 
        first_click_start = managers.player:player_timer():time()
    end    
	return self._steelsight_speed * BowWeaponBase.super.enter_steelsight_speed_multiplier(self)
end